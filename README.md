<h1 align="center">
    <img alt="Banner CompanyDesk" title="#bannercompanydesk" src="./assets/banner.png" />
</h1>
<p style="text-align: center;"> 
<a href="">
<img alt="License" src="https://img.shields.io/badge/license-MIT-brightgreen">
</a>
<a href="">
<img alt="Feito por Patrick Gonçalves" src="https://img.shields.io/badge/feito%20por-Patrick Gonçalves-%237519C1">
</a>
<a href="https://www.companytec.com.br/">
    <img alt="Stargazers" src="https://img.shields.io/badge/Site-Companytec-%237159c1?style=flat&logo=ghost">
</a>
</p>

<h4 align="center"> 
	🚧  CompanyDesk :telephone: Em construção... 🚧
</h4>


## 💻 Sobre o projeto

:telephone: &nbsp;&nbsp;&nbsp;CompanyDesk&nbsp; - &nbsp;é um software voltado ao cadastro dos atendimentos que são realizados na empresa Companytec através do setor de suporte técnico.
Esse projeto foi desenvolvido para um trabalho de conclusão de curso de Análise e Desenvolvimento de Sistemas da Faculdade de Tecnologia Senac Pelotas, onde foi visada a busca por um maior conhecimento da ferramenta principal que seria o framework CodeIgniter e de algumas outras que estão envolvidas no projeto, como GitLab, Visual Studio Code, Ion Auth, etc.

---

## ⚙️ Funcionalidades

- [x] Clientes finais ou técnicos parceiros podem entrar em contato para realizar a abertura de um chamado informando:
  - [x] CNPJ de ambas empresas
  - [x] equipamentos instalados no cliente final
  - [x] detalhar informações de testes e funcionamento dos equipamentos

<br />

- [x] Os usuários, que são os funcionários da empresa prestadora do serviço, devem:
  - [x] coletar as informações e realizar a abertura do chamado de serviço (quando necessitar)
  - [x] realizar a abertura da atividade e anexá-la ao chamado (quando necessáário)
  - [x] realizar a abertura de um atendimento e anexá-lo a devida atividade

---


### :star: Features

- [x] Cadastro de usuário
- [x] Cadastro de cliente final/parceiro de negócios
- [x] Cadastro de modelos de equipamentos
- [x] Cadastro de chamados/atividades/atendimentos
- [x] Indicadores com gráficos
- [x] Alerta de chamados atrasados



### 🎲 Acessando a aplicação
Antes de começar, é preciso acessar a aplicação através do link https://www.companydesk.com.br/ usando o login professores@companydesk.com.br e a senha Professores@2021.

Este usuário tem permissão de administrador, portanto ele terá acesso a todo o sistema.

Para realizar o acesso com um usuário normal, use o login suporte1@companydesk.com.br com a senha Suporte1@2021.

Para verificar o envio de e-mail, acesse o link https://www.companydesk.com.br/webmail usando o mesmo login que foi usado para acessar o sistema.

### 🛠 Tecnologias

As seguintes ferramentas foram usadas na construção do projeto:

-   **[CodeIgniter](https://github.com/bcit-ci/CodeIgniter)**


### 👨 Autor
<img alt="Banner CompanyDesk" title="#bannercompanydesk" src="./assets/author.jpeg" width="100px;" style="border-radius: 50%;"/>
<br />
<sub><b>Patrick Gonçalves</b></sub></a> <a href="" title="patrick">:skull:</a>
<br/>
<br/>


[![Linkedin Badge](https://img.shields.io/badge/-Patrick-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/pgmedeiros/)](https://www.linkedin.com/in/pgmedeiros/) 
[![Gmail Badge](https://img.shields.io/badge/-pgmedeiros93@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:pgmedeiros93@gmail.com)](mailto:pgmedeiros93@gmail.com)

