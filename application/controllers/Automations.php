<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Automations extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
    }

    public function index(){
        $data = array(
            'title' => 'Automações cadastradas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'automations' => $this->core_model->get_all('automations'),         
            'users' => $this->ion_auth->users()->result(),   
        );

        //echo '<pre>';
        //print_r($data['automations']);
        //exit();
        
        $this->load->view('layout/header',$data);
        $this->load->view('automations/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($automation_id = NULL){
        if(!$automation_id || !$this->core_model->get_by_id('automations', array('automations_id' => $automation_id))){
            $this->session->set_flashdata('error','Modelo de automação não encontrado.');
            redirect('automations');
        } else {
            $this->form_validation->set_rules('automations_model','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'automations_model',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('automations', $data, array('automations_id'=>$automation_id));

                redirect('automations');
            }else{

                $data = array(
                    'title' => ' Atualizar modelo de automação',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'automation' => $this->core_model->get_by_id('automations', array('automations_id' => $automation_id)),                    
                );
                $this->load->view('layout/header',$data);
                $this->load->view('automations/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('automations_model','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'automations_model',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('automations', $data);
            redirect('automations');
        }else{
            $data = array(
                'title' => 'Cadastrar modelo de automação',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('automations/add');
            $this->load->view('layout/footer');           
        }
    }

    public function delete($automation_id = NULL){
        if(!$automation_id || !$this->core_model->get_by_id('automations', array('automations_id' => $automation_id))){
            $this->session->set_flashdata('error','Automação não encontrado.');
            redirect('automations');
        } else {
            $this->core_model->delete('automations', array('automations_id'=>$automation_id));
            redirect('automations');
        }
    }
}