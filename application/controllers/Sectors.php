<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Sectors extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
    }

    public function index(){

        $data = array(
            'title' => 'Setores cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'sectors' => $this->core_model->get_all('sectors'),
        );

        /*
        echo '<pre>';
        print_r($data['sectors']);
        exit();
        */
        
        $this->load->view('layout/header',$data);
        $this->load->view('sectors/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($sector_id = NULL){
        if(!$sector_id || !$this->core_model->get_by_id('sectors', array('sectors_id' => $sector_id))){
            $this->session->set_flashdata('error','Setor não encontrado.');
            redirect('sectors');
        } else {
            $this->form_validation->set_rules('sectors_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'sectors_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('sectors', $data, array('sectors_id'=>$sector_id));

                redirect('sectors');
            }else{

                $data = array(
                    'title' => ' Atualizar setor',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'sector' => $this->core_model->get_by_id('sectors', array('sectors_id' => $sector_id)),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('sectors/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('sectors_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'sectors_description',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('sectors', $data);
            redirect('sectors');
        }else{
            $data = array(
                'title' => 'Cadastrar setor',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('sectors/add');
            $this->load->view('layout/footer');           
        }
    }

    public function delete($sector_id = NULL){
        if(!$sector_id || !$this->core_model->get_by_id('sectors', array('sectors_id' => $sector_id))){
            $this->session->set_flashdata('error','Setor não encontrado.');
            redirect('sectors');
        } else {
            $this->core_model->delete('sectors', array('sectors_id'=>$sector_id));
            redirect('sectors');
        }
    }
}