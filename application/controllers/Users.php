<?php
defined ('BASEPATH') OR exit ('Ação não permitida');

class Users extends CI_Controller{   
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }
    }
    public function index(){
        $data = array(
            'title' => 'Usuários cadastrados',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',            
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),            
            'users' => $this->ion_auth->users()->result(),
        );

        //echo '<pre>';
        //print_r($data['usuarios']);
        //exit();

        $this->load->view('layout/header', $data); //passando parâmetro data para a view
        $this->load->view('users/index');
        $this->load->view('layout/footer');
    }

    public function edit($user_id = NULL) {
        if(!$user_id || !$this->ion_auth->user($user_id)->row()){
            $this->session->set_flashdata('error', 'Usuário não encontrado.');
            redirect('users');
        }else{

            $this->form_validation->set_rules('first_name', '', 'trim|required');
            $this->form_validation->set_rules('last_name', '', 'trim|required');
            $this->form_validation->set_rules('email', '', 'trim|required|valid_email|callback_email_check');
            $this->form_validation->set_rules('username', '', 'trim|required|callback_username_check');
            $this->form_validation->set_rules('password', 'Senha', 'min_length[5]|max_length[255]');
            $this->form_validation->set_rules('confirm_password', 'Confirmação de senha', 'matches[password]');


        
            //$this->session->set_flashdata('error', 'Usuário não encontrado.');
            if($this->form_validation->run()){  
                $data = elements(
                    array(
                        'first_name',
                        'last_name',
                        'email',
                        'username',
                        'active',                        
                        'password',
                        //'confirm_password',
                    ),
                    $this->input->post()
                );
                //Comando para limpar array de usuário de códigos inválidos
                $data = $this->security->xss_clean($data);

                /*Verifica se foi passado o password*/ 
                $password = $this->input->post('password');
                if(!$password){
                    unset($data['password']);
                }

                if($this->ion_auth->update($user_id, $data)){
                
                    $profile_user_db = $this->ion_auth->get_users_groups($user_id)->row();

                    $profile_user_post = $this->input->post('profile_user');
                    /*Se for diferente atualiza o grupo que o usuário está cadastrado*/
                    if($profile_user_db->id != $profile_user_post){
                        //$this->ion_auth->remove_from_group($profile_user_db->id, $user_id);                        
                        $this->ion_auth->add_to_group($profile_user_post, $user_id);
                    }

                    $this->session->set_flashdata('success', 'Dados salvos com sucesso.');
                }else {
                    $this->session->set_flashdata('error','Erro ao salvar os dados.');
                }                
                redirect('users');                          
            }else {
                $data = array(
                    'title' => 'Editar usuário',
                    'user' => $this->ion_auth->user($user_id)->row(),
                    'profile_user' => $this->ion_auth->get_users_groups($user_id)->row(),
                );            

                $this->load->view('layout/header', $data);
                $this->load->view('users/edit');
                $this->load->view('layout/footer');
            }     
        }   
    }

    public function admin_check(){
        if (!$this->ion_auth->is_admin()){
            $this->session->set_flashdata('message', 'You must be an admin to view this page');
            redirect('welcome/index');
        }
    }

    public function add(){
       
        $this->form_validation->set_rules('first_name', '', 'trim|required');
        $this->form_validation->set_rules('last_name', '', 'trim|required');
        $this->form_validation->set_rules('email', '', 'trim|required|valid_email|is_unique[users.email]');
        $this->form_validation->set_rules('username', '', 'trim|required|is_unique[users.username]');
        $this->form_validation->set_rules('password', 'Senha', 'required|min_length[5]|max_length[255]');
        $this->form_validation->set_rules('confirm_password', 'Confirmação de senha', 'matches[password]');

        if($this->form_validation->run()){
            $username = $this->security->xss_clean($this->input->post('username'));
            $password = $this->security->xss_clean($this->input->post('password'));
            $email = $this->security->xss_clean($this->input->post('email'));
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'username' => $this->input->post('username'),
                'active' => $this->input->post('active'),
            );
            $group = array(
                $this->input->post('profile_user'),
            );
            $additional_data = $this->security->xss_clean($additional_data);
            $group = $this->security->xss_clean($group);

            if($this->ion_auth->register($username, $password, $email, $additional_data, $group)){
                $this->session->set_flashdata('success', 'Dados salvos com sucesso.');                
            }else{
                $this->session->set_flashdata('error', 'Erro ao salvar dados do usuário.');
            }
            redirect('users');
        } else {
            $data = array(
                'title' => 'Cadastrar usuário',            
            );
            $this->load->view('layout/header', $data);
            $this->load->view('users/add');                
            $this->load->view('layout/footer');
        }        
    }

    public function delete($user_id = NULL){
        if(!$user_id || !$this->ion_auth->user($user_id)->row()){
            $this->session->set_flashdata('error', 'Usuário não encontrado.');
            redirect('users');
        }
        if($this->ion_auth->is_admin($user_id)){
            $this->session->set_flashdata('error', 'O administrador não pode ser excluído.');
            redirect('users');            
        }
        if($this->ion_auth->delete_user($user_id)){
            $this->session->set_flashdata('success', 'Usuário excluído com sucesso.');
            redirect('users');
        } else {
            $this->session->set_flashdata('error', 'O administrador não pode ser excluído.');
            redirect('users');            
        }
    }

    public function email_check($email) {
        $user_id = $this->input->post('user_id');
        if($this->core_model->get_by_id('users', array('email' => $email, 'id !=' => $user_id))) {
            $this->form_validation->set_message('email_check','Este e-mail já existe.');            
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function username_check($username) {
        $user_id = $this->input->post('user_id');
        if($this->core_model->get_by_id('users', array('username' => $username, 'id !=' => $user_id))) {
            $this->form_validation->set_message('username_check','Este usuário já existe.');            
            return FALSE;
        }else{
            return TRUE;
        }
    }

    public function showDescription($user_id = NULL){
        $data = array(
            'title' => 'Descrição do usuário',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css',   
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',
            ),
            'user' => $this->core_model->get_by_id('users', array('id' => $user_id)),
        );

        $this->load->view('layout/header', $data);
        $this->load->view('users/showDescription');
        $this->load->view('layout/footer');
    }
}
            

//          echo '<pre>';
//          print_r($this->input->post());
//          exit();

    /*
    [first_name] => Admin
    [last_name] => istrator
    [email] => admin@admin.com
    [username] => administrator
    [active] => 1
    [perfil_user] => 1
    [password] => 
    [confirm_password] => 
    [user_id] => 1
    */                                   