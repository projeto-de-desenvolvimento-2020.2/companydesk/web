<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Pumpsbrand extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
    }

    public function index(){

        $data = array(
            'title' => 'Marcas de bombas cadastradas',
            'styles' => array(
                'vendor/datatables/dataTables.bootstrap4.min.css',
            ),
            'scripts' => array(
                'vendor/datatables/jquery.dataTables.min.js',
                'vendor/datatables/dataTables.bootstrap4.min.css', 
                'vendor/datatables/dataTables.bootstrap4.min.js',
                'vendor/datatables/app.js',                
            ),
            'pumps_brand' => $this->core_model->get_all('pumps_brand'),
        );

        /*
        echo '<pre>';
        print_r($data['pumps_brand']);
        exit();
        */
        
        $this->load->view('layout/header',$data);
        $this->load->view('pumpsbrand/index');
        $this->load->view('layout/footer');
    }
    
    public function edit($pump_brand_id = NULL){
        if(!$pump_brand_id || !$this->core_model->get_by_id('pumps_brand', array('pumps_brand_id' => $pump_brand_id))){
            $this->session->set_flashdata('error','Marca de bomba não encontrada.');
            redirect('pumpsbrand');
        } else {
            $this->form_validation->set_rules('pumps_brand_description','','trim|required');            
            if($this->form_validation->run()){
                $data = elements(
                    array(         
                        'pumps_brand_description',
                    ),
                    $this->input->post()
                );
                $data = html_escape($data);
                $this->core_model->update('pumps_brand', $data, array('pumps_brand_id'=>$pump_brand_id));

                redirect('pumpsbrand');
            }else{

                $data = array(
                    'title' => ' Atualizar marca de bomba',
                    'scripts' => array(
                        'vendor/mask/jquery.mask.min.js',
                        'vendor/mask/app.js',
                    ),
                    'pump_brand' => $this->core_model->get_by_id('pumps_brand', array('pumps_brand_id' => $pump_brand_id)),
                );
                $this->load->view('layout/header',$data);
                $this->load->view('pumpsbrand/edit');
                $this->load->view('layout/footer');
            }            
        }
    }

    public function add(){        
        $this->form_validation->set_rules('pumps_brand_description','','trim|required');
        if($this->form_validation->run()){
            $data = elements(
                array(   
                    'pumps_brand_description',                    
                ),
                $this->input->post()
            );
            $data = html_escape($data);
            $this->core_model->insert('pumps_brand', $data);
            redirect('pumpsbrand');
        }else{
            $data = array(
                'title' => 'Cadastrar marca de bomba',
                'scripts' => array(
                    'vendor/mask/jquery.mask.min.js',
                    'vendor/mask/app.js',
                ),                  
            );
            $this->load->view('layout/header',$data);
            $this->load->view('pumpsbrand/add');
            $this->load->view('layout/footer');           
        }
    }
    public function delete($pump_brand_id  = NULL){
        if(!$pump_brand_id  || !$this->core_model->get_by_id('pumps_brand', array('pumps_brand_id' => $pump_brand_id ))){
            $this->session->set_flashdata('error','Marca de bomba não encontrada.');
            redirect('pumpsbrand');
        } else {
            $this->core_model->delete('pumps_brand', array('pumps_brand_id'=>$pump_brand_id ));
            redirect('pumpsbrand');
        }
    }
}