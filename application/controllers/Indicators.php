<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Indicators extends CI_Controller{
    public function __construct(){
        parent::__construct();
        if(!$this->ion_auth->logged_in()){
            $this->session->set_flashdata('info','Seu sessão expirou.');
            redirect('login');
        }        
        $this->load->database();
        $this->load->model('indicators_model');
    }

    public function index(){
        $month = @date('m');

        $query = $this->db->query("SELECT count(calleds_id) as count_calleds, MONTH(calleds_start) FROM calleds GROUP BY MONTH(calleds_start) ORDER BY calleds_start DESC LIMIT 6"); 
        $data['calleds'] = json_encode(array_column($query->result(), 'count_calleds'),JSON_NUMERIC_CHECK);

        $query = $this->db->query("SELECT MONTH(calleds_start) as months_calleds FROM calleds GROUP BY MONTH(calleds_start) ORDER BY calleds_start DESC LIMIT 6"); 
        $data['months'] = json_encode(array_column($query->result(), 'months_calleds'),JSON_NUMERIC_CHECK);

        // $query = $this->db->query("SELECT count(calleds_id) as count_calleds, MONTH(calleds_start) FROM calleds GROUP BY MONTH(calleds_start) ORDER BY calleds_start DESC LIMIT 6"); 
        // $data['activities'] = json_encode(array_column($query->result(), 'count_activities'),JSON_NUMERIC_CHECK);

        $query = $this->db->query("
            SELECT COUNT(calleds.calleds_id) as calleds, calleds.calleds_users_id, users.first_name as users
            FROM calleds 
            LEFT JOIN users
            ON calleds.calleds_users_id = users.id
            WHERE MONTH(calleds.calleds_start) = ".$month." GROUP BY calleds.calleds_users_id        
        "); 
        $data['usersCalleds'] = json_encode(array_column($query->result(), 'users'),JSON_NUMERIC_CHECK);
        $data['calledCalleds'] = json_encode(array_column($query->result(), 'calleds'),JSON_NUMERIC_CHECK);
        //echo '<pre>';
        //print_r($data['activities']);
        //exit();

        $this->load->view('layout/header', $data);
        $this->load->view('indicators/index');
        $this->load->view('layout/footer');
    }
}