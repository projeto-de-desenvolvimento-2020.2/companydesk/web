<?php $this->load->view('layout/sidebar'); ?>



<!-- Main Content -->
<div id="content">

    <?php $this->load->view('layout/navbar');?>

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a title="Cadastrar novo atendimento" href="<?php echo base_url('attendances/add')?>"
                    class=" btn btn-success btn-sm float-right"><i class="fas fa-user-plus"></i>&nbsp; Novo</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Inicio</th>
                                <th class="text-center">Final</th>
                                <th class="text-center">Atividade</th>
                                <th class="text-center">Chamado</th>   
                                <th class="text-center">Cliente</th>                               
                                <th class="text-center">Parceiro</th>                                  
                                <th class="text-center">Usuário</th>
                                <th class="text-center">Origem</th>
                                <th class="text-center no-sort">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($attendances as $attendance):?>
                            <tr>
                                <td class="text-center"> <?php echo $attendance->attendances_id ?></td>
                                <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($attendance->attendances_start)) ?></td>
                                <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($attendance->attendances_end)) ?></td>
                                <td class="text-center"> <?php echo $attendance->attendances_activities_id ?></td>
                                <td class="text-center"> <?php echo $attendance->attendances_calleds_id ?></td>
                                <td class="text-center"> <?php echo $attendance->attendances_customers_end_id ?></td>
                                <td class="text-center"> <?php echo $attendance->attendances_customers_partner_id ?></td> 
                                <td class="text-center"> <?php echo $attendance->attendances_users_id ?></td>
                                <td class="text-center"> <?php echo $attendance->attendances_origins_id ?></td>
                                <td class="text-center">
                                    <a title="Editar" href="<?php echo base_url('attendances/edit/'.$attendance->attendances_id); ?>"
                                        class="btn brn-sm btn-primary"><i class="fas fa-edit"></i></a>                                    
                                </td>
                            </tr>                           
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>