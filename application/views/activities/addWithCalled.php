<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>

    <div class="container-fluid">

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('activities'); ?>">Atividades</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_add_activity" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Cliente</label>
                                <input type="text" class="form-control" id="activities_customers_end_id_ac"
                                value="<?php echo set_value('activities_customers_end_id');?>">
                                <?php echo form_error('activities_customers_end_id','<small class="form-text text-danger">','</small>')?>
                                <input type="hidden" name="activities_customers_end_id" id="activities_customers_end_id">                            
                        </div>
                        <div class="col-md-3">
                            <label>Parceiro</label>
                                <input type="text" class="form-control" id="activities_customers_partner_id_ac"
                                    value="<?php echo set_value('activities_customers_partner_id');?>">
                                <?php echo form_error('activities_customers_partner_id','<small class="form-text text-danger">','</small>')?>      
                                <input type="hidden" name="activities_customers_partner_id" id="activities_customers_partner_id">                           
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="attendances_users_id" disabled>
                                    <?php $user = $this->ion_auth->user()->row(); ?>
                                    <option value="<?php echo $user->id ?>"><?php echo $user->first_name;?></option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Origem</label>
                            <select class="custom-select" name="activities_origins_id">
                                <?php foreach($origins as $origin): ?>
                                    <option value="<?php echo $origin->origins_id ?>"><?php echo $origin->origins_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>                        
                    </div>                    
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Chamado</label>
                            <input type="text" class="form-control" name="activities_calleds_id" readonly
                                    value="<?php echo $called_id;?>">
                                <?php echo form_error('activities_calleds_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Prioridade</label>
                            <select class="custom-select" name="activities_priorities_id">
                                <?php foreach($priorities as $priority): ?>
                                    <option value="<?php echo $priority->priorities_id ?>"> <?php echo $priority->priorities_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Automação</label>
                            <select class="custom-select" name="activities_automations_id">
                                <?php foreach($automations as $automation): ?>
                                    <option value="<?php echo $automation->automations_id ?>"><?php echo $automation->automations_model ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Identificador</label>
                            <select class="custom-select" name="activities_types_identifier_id">
                                <?php foreach($types_identifier as $type_identifier): ?>
                                    <option value="<?php echo $type_identifier->types_identifier_id ?>"><?php echo $type_identifier->types_identifier_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Tipo</label>
                            <select class="custom-select" name="activities_call_types_id">
                                <?php foreach($call_types as $call_type): ?>
                                    <option value="<?php echo $call_type->call_types_id ?>"><?php echo $call_type->call_types_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label>Status</label>
                            <select class="custom-select" name="activities_status_id">
                                <?php foreach($status as $statu): ?>
                                    <option value="<?php echo $statu->status_id ?>"><?php echo $statu->status_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>  
                        <!-- <div class="col-md-4">
                            <div class="form-group" enctype="multipart/form-data" >
                                <label for="exampleFormControlFile1">Anexos</label>
                                <input type="file" class="form-control-file" id="exampleFormControlFile1" name="activities_atachments">
                            </div>
                        </div> -->
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Início</label>
                            <input type="datetime-local" class="form-control" name="activities_start" 
                                value="<?php echo set_value('activities_start');?>">
                            <!-- value é o que vem lá do banco e o user é o que vem do controller-->
                            <?php echo form_error('activities_start','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-6">
                            <label>Final</label>
                            <input type="datetime-local" class="form-control" name="activities_end"
                                value="<?php echo set_value('activities_end');?>">
                            <!-- value é o que vem lá do banco e o user é o que vem do controller-->
                            <?php echo form_error('activities_end','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="activities_description"
                                rows="12"><?php echo set_value('activities_description');?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-sm ml-3"><i class="far fa-save"></i>&nbsp;&nbsp; Salvar</button>
                        <a title="Voltar" href="<?php echo base_url('calleds/showDescription/'.$called_id);?>"
                            class=" btn btn-success btn-sm float-right ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>