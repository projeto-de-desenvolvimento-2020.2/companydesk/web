<?php $this->load->view('layout/sidebar');?>
<div id="content">
<?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Informações</h1>
        </div>
            <div class="row">
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Atendimentos (Últimos 30 dias)</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    <?php foreach ($attendancesLast30Days as $numberAttendace) { ?>
                                        <?php echo $numberAttendace->numberAttendances?>
                                    <?php } ?>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-900"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                        Atividades (Últimos 30 dias)</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            <?php foreach($activitiesLast30Days as $numberActivity){ ?>
                                                <?php echo $numberActivity->numberActivities ?>
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-900"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Chamados (Últimos 30 dias)</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <?php foreach($calledsLast30Days as $numberCalled){ ?>
                                                        <?php echo $numberCalled->numberCalleds ?>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-900"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            <div class="row">
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Atendimentos (Hoje)</div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                                    <?php foreach ($attendancesIndexToday as $attendanceToday) { ?>
                                        <?php echo $attendanceToday->attendancesToday?>
                                    <?php } ?>
                                    </div>
                                </div>
                                <div class="col-auto">
                                    <i class="fas fa-calendar fa-2x text-gray-900"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6 mb-4">
                    <div class="card border-left-success shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div class="text-xs font-weight-bold text-success text-uppercase mb-1">
                                        Atividades (Hoje)</div>
                                        <div class="h5 mb-0 font-weight-bold text-gray-800">
                                            <?php foreach($activitiesIndexToday as $activityToday){ ?>
                                                <?php echo $activityToday->activitiesToday ?>
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-calendar fa-2x text-gray-900"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">
                                                Chamados (Hoje)</div>
                                                <div class="h5 mb-0 font-weight-bold text-gray-800">
                                                    <?php foreach($calledsIndexToday as $calledToday){ ?>
                                                        <?php echo $calledToday->calledsToday ?>
                                                    <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-900"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
            <div class="row">
                <div class="col-lg-6 mb-4">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-success">Últimos chamados abertos</h6>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                            <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 21rem;"
                                src="public/img/helpdesk.svg" alt="...">
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-borderless"> 
                                    <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Início</th>
                                            <th>Cliente</th>
                                            <th>Parceiro</th>
                                            <th>Usuário</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($lastThreeCalleds as $called):?>
                                            <tr>
                                                <td><?php echo $called->calleds_id ?></td>
                                                <td><?php echo $called->calleds_start ?></td>
                                                <td><?php echo $called->customers_end ?></td>
                                                <td><?php echo $called->customers_partner ?></td>
                                                <td><?php echo $called->user ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 mb-4">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Chamados abertos e fechados hoje</h6>
                        </div>
                        <div class="card-body">
                            <div class="text-center">
                            <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 35rem;"
                                src="public/img/workspace.svg" alt="...">
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-borderless"> 
                                    <thead>
                                        <tr>
                                            <th>Abertos</th>
                                            <th>Fechados</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                                <td>
                                                    <?php foreach($calledsIndexOpenToday as $calledOpToday){ ?>
                                                        <?php echo $calledOpToday->calledsOpeToday ?>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php foreach($calledsIndexCloseToday as $calledClToday){ ?>
                                                        <?php echo $calledClToday->calledsClosToday ?>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div>
</div>