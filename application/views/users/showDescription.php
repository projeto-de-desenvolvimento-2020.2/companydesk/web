<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('users'); ?>">Usuários</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">
            
            <div class="card-body">
                <form method="POST" name="form_add">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="first_name"
                                value="<?php echo $user->first_name;?>" readonly>
                            <?php echo form_error('first_name','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Sobrenome</label>
                            <input type="text" class="form-control" name="last_name"
                                value="<?php echo $user->last_name;?>" readonly>
                            <?php echo form_error('last_name','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="email"
                                value="<?php echo $user->email;?>" readonly>
                            <?php echo form_error('email','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Usuário</label>
                            <input type="text" class="form-control" name="username"
                                value="<?php echo $user->username;?>" readonly>
                            <?php echo form_error('username','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Ativo</label>
                            <select class="custom-select" name="active" disabled>
                                <option value=""><?php echo($user->active == 0 ? 'Não' : 'Sim')?></option>  
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Perfil de acesso</label>
                            <select class="form-control" name="profile_user" disabled>
                                <option value="2">Normal
                                </option>
                                <option value="1">Administrator
                                </option>
                            </select>
                        </div>
                    </div>
                    <a title="Voltar" href="<?php echo base_url('users');?>" class=" btn btn-success btn-sm float-left"><i
                            class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                </div>
            </form>
        </div>
    </div>
</div>