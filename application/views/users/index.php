<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <!-- Mensagens informadas ao topo da janela -->
        <?php if($message = $this->session->flashdata('success')):?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;<?php echo $message;?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php endif;?>
        <?php if($message = $this->session->flashdata('error')):?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;<?php echo $message;?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php endif;?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a title="Cadastrar novo usuário" href="<?php echo base_url('users/add')?>"
                    class=" btn btn-success btn-sm float-right"><i class="fas fa-user-plus"></i>&nbsp Novo</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Usuário</th>
                                <th class="text-center">Login</th>
                                <th class="text-center">Perfil</th>
                                <th class="text-center">Ativo</th>
                                <th class="text-center no-sort">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($users as $user):?>
                            <tr>
                                <td class="text-center"> <?php echo $user->id ?></td>
                                <td class="text-center"> <?php echo $user->username ?></td>
                                <td class="text-center"> <?php echo $user->email ?></td>
                                <td class="text-center"> <?php echo ($this->ion_auth->is_admin($user->id) ? 'Administrador' : 'Usuário'); ?>
                                </td>
                                <td class="text-center pr-4"> <?php echo ($user->active == 1 ? "Sim":"Não") ?></td>
                                <td class="text-center">
                                        <a title="Editar" href="<?php echo base_url('users/edit/'.$user->id); ?>"
                                                class="btn brn-sm btn-primary"><i class="fas fa-user-edit"></i></a>
                                        <a title="Excluir" href="javascript(void)" data-toggle="modal" data-target="#user-<?php echo $user->id; ?>"
                                                class="btn brn-sm btn-danger"><i class="fas fa-user-times"></i></a>
                                </td>
                            </tr>
                            <div class="modal fade" id="user-<?php echo $user->id; ?>" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Deseja realmente excluir usuário?</h5>
                                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">Se deseja realmente deletar, aperte o botão sim.</div>
                                        <div class="modal-footer">
                                            <button class="btn btn-secondary btn-sm" type="button"
                                                data-dismiss="modal">Não</button>
                                            <a class="btn btn-danger btn-sm" href="<?php echo base_url('users/delete/'.$user->id);?>">Sim</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>