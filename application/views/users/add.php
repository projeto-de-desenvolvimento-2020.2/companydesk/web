<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('users'); ?>">Usuários</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a title="Voltar" href="<?php echo base_url('users');?>" class=" btn btn-success btn-sm float-right"><i
                        class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
            </div>
            <div class="card-body">
                <form method="POST" name="form_add">
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Nome</label>
                            <input type="text" class="form-control" name="first_name" placeholder="Nome do usuário"
                                value="<?php echo set_value('first_name');?>">
                            <!-- value é o que vem lá do banco e o user é o que vem do controller-->
                            <?php echo form_error('first_name','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Sobrenome</label>
                            <input type="text" class="form-control" name="last_name" placeholder="Sobrenome do usuário"
                                value="<?php echo set_value('last_name');?>">
                            <?php echo form_error('last_name','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="email" placeholder="E-mail do usuário"
                                value="<?php echo set_value('email');?>">
                            <?php echo form_error('email','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Usuário</label>
                            <input type="text" class="form-control" name="username" placeholder="usuário" value="<?php echo set_value('username');?>">
                            <?php echo form_error('username','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Ativo</label>
                            <select class="form-control" name="active">
                                <option value="0">Não</option>
                                <option value="1">Sim</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Perfil de acesso</label>
                            <select class="form-control" name="profile_user">
                                <option value="2">Normal
                                </option>
                                <option value="1">Administrator
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Senha do usuário</label>
                            <input type="password" class="form-control" name="password" placeholder="Senha">
                            <?php echo form_error('password','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-6">
                            <label>Confirmação de senha</label>
                            <input type="password" class="form-control" name="confirm_password"
                                placeholder="Confirme sua senha">
                            <?php echo form_error('confirm_password','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
            </div>
            </form>
        </div>
    </div>
</div>