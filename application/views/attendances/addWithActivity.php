<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('attendances'); ?>">Atendimentos</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_add_attendance">                    
                    <div class="form-group row">
                        <div class="col-md-4">
                            <label>Cliente</label>
                                <input type="text" class="form-control" id="attendances_customers_end_id_ac"
                                    value="<?php echo set_value('attendances_customers_end_id');?>">
                                <?php echo form_error('attendances_customers_end_id','<small class="form-text text-danger">','</small>')?> 
                                <input type="hidden" name="attendances_customers_end_id" id="attendances_customers_end_id">                            
                               
                        </div>
                        <div class="col-md-4">
                            <label>Parceiro</label>
                                <input type="text" class="form-control" id="attendances_customers_partner_id_ac"
                                    value="<?php echo set_value('attendances_customers_partner_id');?>">
                                <?php echo form_error('attendances_customers_partner_id','<small class="form-text text-danger">','</small>')?>
                                <input type="hidden" name="attendances_customers_partner_id" id="attendances_customers_partner_id">
                        </div>                        
                        <div class="col-md-4">                        
                            <label>Atividade</label>
                                <input type="text" class="form-control" name="attendances_activities_id" readonly
                                    value="<?php echo $activity_id;?>">
                                <?php echo form_error('attendances_activities_id','<small class="form-text text-danger">','</small>')?>
                        </div>                        
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Origem</label>
                            <select class="custom-select" name="attendances_origins_id">
                                <?php foreach($origins as $origin): ?>
                                    <option value="<?php echo $origin->origins_id ?>"><?php echo $origin->origins_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="attendances_users_id" disabled>
                                    <?php $user = $this->ion_auth->user()->row(); ?>
                                    <option value="<?php echo $user->id ?>"><?php echo $user->first_name;?></option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Início</label>
                            <input type="datetime-local" class="form-control" name="attendances_start" 
                                value="<?php echo set_value('attendances_start');?>">
                            <?php echo form_error('attendances_start','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Final</label>
                            <input type="datetime-local" class="form-control" name="attendances_end" 
                                value="<?php echo set_value('attendances_end');?>">
                            <?php echo form_error('attendances_end','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="attendances_description"
                                rows="12"><?php echo set_value('attendances_description');?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-sm ml-3"><i class="far fa-save"></i>&nbsp;&nbsp; Salvar</button>
                        <a title="Voltar" href="<?php echo base_url('activities/showDescription/'.$activity_id);?>"
                            class=" btn btn-success btn-sm float-right ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                    </div>            
                </form>
            </div>
        </div>
    </div>
</div>