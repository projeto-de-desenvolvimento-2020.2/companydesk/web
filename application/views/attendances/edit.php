<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('attendances'); ?>">Atendimentos</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_edit_attendance">                   
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Cliente</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="attendances_customers_end_id"
                                    placeholder="Cliente" value="<?php echo $attendance->attendances_customers_end_id;?>">
                                <?php echo form_error('attendances_customers_end_id','<small class="form-text text-danger">','</small>')?>
                                <div class="input-group-btn">
                                    <a title="search" href="<?php echo base_url('customers/show_all_customers_calleds');?>"class="btn btn-outline-primary"><i class="fas fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Parceiro</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="attendances_customers_partner_id"
                                    placeholder="Parceiro" value="<?php echo $attendance->attendances_customers_partner_id;?>">
                                <?php echo form_error('attendances_customers_partner_id','<small class="form-text text-danger">','</small>')?>
                                <div class="input-group-btn">
                                    <a title="search" href="<?php echo base_url('customers/show_all_customers_calleds');?>"class="btn btn-outline-primary"><i class="fas fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Atividade</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="attendances_activities_id" placeholder="Atividade"
                                    value="<?php echo $attendance->attendances_activities_id;?>">
                                <?php echo form_error('attendances_activities_id','<small class="form-text text-danger">','</small>')?>
                                <div class="input-group-btn">
                                    <a title="search" href="<?php echo base_url('customers/show_all_customers_calleds');?>"class="btn btn-outline-primary"><i class="fas fa-search"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="attendances_users_id">
                                <?php foreach($users as $user):?>
                                    <option value="<?php echo $user->id ?>"  <?php echo ($user->id == $attendance->attendances_users_id ? 'selected' : '') ?>><?php echo $user->first_name ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Origem</label>
                            <select class="custom-select" name="attendances_origins_id">
                                <?php foreach($origins as $origin):?>
                                    <option value="<?php echo $origin->origins_id ?>"  <?php echo ($origin->origins_id == $attendance->attendances_origins_id ? 'selected' : '') ?>><?php echo $origin->origins_description ?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="attendances_description" rows="12"><?php echo $attendance->attendances_description;?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-sm"><i class="far fa-save"></i>&nbsp;&nbsp;Salvar</button>                          
                        <a title="Voltar" href="<?php echo base_url('attendances');?>"
                            class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                    </div>            
                </form>
            </div>
        </div>
    </div>
</div>