<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Inicio</th>
                                <th class="text-center">Final</th>
                                <th class="text-center">Cliente</th>
                                <th class="text-center">Parceiro</th>
                                <th class="text-center">Usuário</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Prioridade</th>
                                <th class="text-center">Tipo de chamado</th>
                                <th class="text-center no-sort">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($calleds as $called):?>
                            <tr>
                                <td class="text-center"> <?php echo $called->calleds_id ?></td>
                                <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($called->calleds_start)) ?></td>
                                <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($called->calleds_end)) ?></td>
                                <td class="text-center"> <?php echo $called->calleds_customers_end_id ?></td>
                                <td class="text-center"> <?php echo $called->calleds_customers_partner_id ?></td> 
                                <td class="text-center"> <?php echo $called->calleds_users_id?></td>
                                <td class="text-center"> <?php echo $called->calleds_status_id ?></td>
                                <td class="text-center"> <?php 
                                    if($called->calleds_priorities_id == 1){
                                        echo '<span class="badge badge-danger btn-sm">'.$called->calleds_priorities_id.'</span>';
                                    }else if($called->calleds_priorities_id == 2){
                                        echo '<span class="badge badge-success btn-sm">'.$called->calleds_priorities_id.'</span>';
                                    }else{
                                        echo '<span class="badge badge-primary btn-sm">'.$called->calleds_priorities_id.'</span>';
                                    } 
                                ?></td>
                                <td class="text-center"> <?php echo $called->calleds_call_types_id ?></td>
                                <td class="text-center">
                                    <a title="Editar"
                                        href="<?php echo base_url('calleds/edit/'.$called->calleds_id); ?>"
                                        class="btn brn-sm btn-primary"><i class="fas fa-edit"></i></a>                                    
                                </td>
                            </tr>                        
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>