<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('customers'); ?>">Clientes</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form method="POST" name="form_add_customers">
                    
                    <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-user-tie"></i>&nbsp; Dados empresariais</legend>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Razão Social</label>
                            <input type="text" class="form-control" name="customers_social_reason"
                                value="<?php echo set_value('customers_social_reason');?>">
                            <?php echo form_error('customers_social_reason','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Nome Fantasia</label>
                            <input type="text" class="form-control" name="customers_fantasy_name"
                                 value="<?php echo set_value('customers_fantasy_name');?>">
                            <?php echo form_error('customers_fantasy_name','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>CNPJ</label>
                            <input type="text" class="form-control cnpj" name="customers_cnpj"
                                value="<?php echo set_value('customers_cnpj');?>">
                            <?php echo form_error('customers_cnpj','<small class="form-text text-danger">','</small>')?>
                        </div>
                          <div class="col-md-3">
                            <label>Inscrição estadual</label>
                            <input type="text" class="form-control" name="customers_state_registration"
                                value="<?php echo set_value('customers_state_registration');?>">
                            <?php echo form_error('customers_state_registration','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-4">
                            <label>Telefone fixo</label>
                            <input type="text" class="form-control phone_with_ddd" name="customers_telephone_fix"
                                value="<?php echo set_value('customers_telephone_fix');?>">
                            <?php echo form_error('customers_telephone_fix','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Telefone móvel</label>
                            <input type="text" class="form-control cel_phone_with_ddd" name="customers_telephone"
                                 value="<?php echo set_value('customers_telephone');?>">
                            <?php echo form_error('customers_telephone','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="customers_email" 
                                value="<?php echo set_value('customers_email')?>">
                            <?php echo form_error('customers_email','<small class="form-text text-danger">','</small>')?>
                        </div>                          
                    </div>
                </fieldset>
                <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-map-marker-alt"></i>&nbsp; Dados de endereço</legend>
                    <div class="form-group row mt-3">
                        <div class="col-md-6">
                            <label>Endereço</label>
                            <input type="text" class="form-control" name="customers_address"
                                value="<?php echo set_value('customers_address');?>">
                            <?php echo form_error('customers_address','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-2">
                            <label>Número</label>
                            <input type="text" class="form-control" name="customers_address_number"
                                placeholder="Número" value="<?php echo set_value('customers_address_number');?>">
                            <?php echo form_error('customers_address_number','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Complemento</label>
                            <input type="text" class="form-control" name="customers_address_complement"
                                value="<?php echo set_value('customers_address_complement');?>">
                            <?php echo form_error('customers_address_complement','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                          <div class="col-md-3">
                            <label>Cidade</label>
                            <input type="text" class="form-control" name="customers_city"
                                value="<?php echo set_value('customers_city');?>">
                            <?php echo form_error('customers_city','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>CEP</label>
                            <input type="text" class="form-control cep" name="customers_cep"
                                value="<?php echo set_value('customers_cep');?>">
                            <?php echo form_error('customers_cep','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>UF</label>
                            <select class = "form-control form-control-user" name="customers_state">
                                <option value="AC">Acre</option>
                                <option value="AL">Alagoas</option>
                                <option value="AP">Amapá</option>
                                <option value="AM">Amazonas</option>
                                <option value="BA">Bahia</option>
                                <option value="CE">Ceará</option>
                                <option value="DF">Distrito Federal</option>   
                                <option value="ES">Espírito Santo</option>
                                <option value="GO">Goiás</option>
                                <option value="MA">Maranhão</option>
                                <option value="MT">Mato Grosso</option>
                                <option value="MS">Mato Grosso Do Sul</option>
                                <option value="MG">Minas Gerais</option>
                                <option value="PA">Pará</option>
                                <option value="PB">Paraíba</option>
                                <option value="PR">Paraná</option>
                                <option value="PE">Pernambuco</option>
                                <option value="PI">Piauí</option>   
                                <option value="RJ">Rio De Janeiro</option>   
                                <option value="RN">Rio Grande Do Norte</option>   
                                <option value="RS" selected>Rio Grande Do Sul</option>   
                                <option value="RO">Rondônia</option>   
                                <option value="RR">Roraima</option>   
                                <option value="SC">Santa Catarina</option>   
                                <option value="SP">São Paulo</option>   
                                <option value="SE">Sergipe</option>   
                                <option value="TO">Tocantins</option>   
                            </select>
                        </div>                                             
                        <div class="col-md-3">
                            <label>País</label>
                            <input type="text" class="form-control" name="customers_country"
                                value="<?php echo set_value('customers_country');?>">
                            <?php echo form_error('customers_country','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-info"></i>&nbsp; Informações</legend>
                    <div class="form-group row mt-3">
                        <div class="col-md-4">
                            <label>Ativo</label>                                                        
                            <select class = "form-control form-control-user" name="customers_defaulting">
                                <option value="0">Não</option>
                                <option value="1" selected>Sim</option>   
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label>Número de bombas</label>
                            <input type="text" class="form-control" name="customers_number_pumps"
                                value="<?php echo set_value('customers_number_pumps');?>">
                            <?php echo form_error('customers_number_pumps','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Tipo de automação</label>
                            <input type="text" class="form-control" name="customers_automations_id"
                                value="<?php echo set_value('customers_automations_id');?>">
                            <?php echo form_error('customers_automations_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                          <div class="col-md-4">
                            <label>Tipo de identificador</label>
                            <input type="text" class="form-control" name="customers_types_identifier_id"
                                value="<?php echo set_value('customers_types_identifier_id');?>">
                            <?php echo form_error('customers_types_identifier_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Marcas das bombas</label>
                            <input type="text" class="form-control" name="customers_pumps_brand_id"
                                value="<?php echo set_value('customers_pumps_brand_id');?>">
                            <?php echo form_error('customers_pumps_brand_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Tipo de cliente</label>
                            <input type="text" class="form-control" name="customers_types_company_id"
                                value="<?php echo set_value('customers_types_company_id');?>">
                            <?php echo form_error('customers_types_company_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                </fieldset>    
                <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-comments"></i>&nbsp; Observações</legend>
                    <div class="form-group row mt-3 mb-3">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="calleds_description"
                                rows="12"><?php echo set_value('customers_comments');?></textarea>
                                <?php echo form_error('customers_comments','<small class="form-text text-danger">','</small>')?>    
                        </div>
                    </div>
                </fieldset>  
                    <button type="submit" class="btn btn-primary btn-sm mt-3"><i class="far fa-save"></i>&nbsp;&nbsp;Salvar</button>                      
                    <a title="Voltar" href="<?php echo base_url('customers');?>"
                        class=" btn btn-success btn-sm ml-3 mt-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
            </div>
        </form>
    </div>
</div>
