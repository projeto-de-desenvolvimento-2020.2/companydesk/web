<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <?php if($message = $this->session->flashdata('success')):?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;<?php echo $message;?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php endif;?>
        <?php if($message = $this->session->flashdata('error')):?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong><i class="fas fa-exclamation-triangle"></i>&nbsp;&nbsp;<?php echo $message;?></strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>
        </div>
        <?php endif;?>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a title="Cadastrar novo cliente final" href="<?php echo base_url('customers/add')?>"
                    class=" btn btn-success btn-sm float-right"><i class="fas fa-user-plus"></i>&nbsp; Novo</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Razão social</th>
                                <th class="text-center">Nome fantasia</th>
                                <th class="text-center">CNPJ</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Telefone fixo</th>
                                <th class="text-center">Telefone móvel</th>
                                <th class="text-center">Estado</th>                            
                                <th class="text-center">Ativo</th>                            
                                <th class="text-center no-sort">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($customers as $customer):?>
                            <tr>
                                <td class="text-center"> <?php echo $customer->customers_id ?></td>
                                <td class="text-center"> <?php echo $customer->customers_social_reason?></td>
                                <td class="text-center"> <?php echo $customer->customers_fantasy_name ?></td>
                                <td class="text-center"> <?php echo $customer->customers_cnpj ?></td>
                                <td class="text-center"> <?php echo $customer->customers_email ?></td>
                                <td class="text-center"> <?php echo $customer->customers_telephone_fix ?></td>
                                <td class="text-center"> <?php echo $customer->customers_telephone ?></td>
                                <td class="text-center"> <?php echo $customer->customers_federative_unit ?></td>                                
                                <td class="text-center pr-4"> <?php echo ($customer->customers_defaulting == 1 ? '<span class="badge badge-primary btn-sm">Sim</span>':'<span class="badge badge-danger">Não</span>') ?></td>
                                <td class="text-center">
                                    <button class="btn brn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-bars"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <?php if($customer->customers_defaulting == 1){ ?>
                                            <a class="dropdown-item" title="Novo chamado" href="<?php echo ($customer->customers_defaulting == 1 ? base_url('customers/newCalled/'.$customer->customers_id) : null);?>">Novo chamado</a>  
                                        <?php } ?>
                                        <a class="dropdown-item" title="Editar" href="<?php echo base_url('customers/edit/'.$customer->customers_id); ?>">Editar</a>                                       
                                        <a class="dropdown-item" tittle="Chamados relacionados" href="<?php echo base_url('customers/relatedCalleds/'.$customer->customers_id);?>">Chamados relacionados</a>
                                    </div>
                                </td>
                            </tr>                        
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>