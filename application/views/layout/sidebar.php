     
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">            
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('home'); ?>">  
            <i class="fas fa-desktop fa-2x"></i>              
                <div class="sidebar-brand-text mx-3">Companydesk</div>
            </a>            
            <hr class="sidebar-divider my-0">            
            <li class="nav-item active">
                <a class="nav-link" href="<?php echo base_url('home'); ?>">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>            
            <hr class="sidebar-divider">            
            <li class="nav-item">
                <a class="nav-link collapsed" href="<?php echo base_url('customers'); ?>" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
                    aria-controls="collapseTwo">                    
                    <i class="fas fa-user"></i>
                    <span>Clientes</span>
                </a>
               <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Opções:</h6>
                        <a class="collapse-item" href="<?php echo base_url('customers'); ?>" ><i class="fas fa-list"></i>&nbsp;&nbsp;&nbsp;Listagem</a>
                        <a class="collapse-item" href="<?php echo base_url('customers/add'); ?>"><i class="fas fa-user-plus"></i>&nbsp;&nbsp;&nbsp;Adicionar</a>
                    </div>
                </div>
            </li>

            <hr class="sidebar-divider">

            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                    aria-expanded="true" aria-controls="collapseUtilities">
                    <i class="fas fa-fw fa-wrench"></i>
                    <span>Serviços</span>
                </a>
                <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">   
                        <h6 class="collapse-header">Opções:</h6>                     
                        <a class="collapse-item" href="<?php echo base_url('calleds'); ?>"><i class="fas fa-phone-alt"></i>&nbsp;&nbsp;&nbsp;Chamados de serviço</a>
                        <a class="collapse-item" href="<?php echo base_url('activities'); ?>"><i class="fas fa-calendar-week"></i>&nbsp;&nbsp;&nbsp;Atividades</a>
                        <a class="collapse-item" href="<?php echo base_url('attendances'); ?>"><i class="fas fa-headset"></i>&nbsp;&nbsp;&nbsp;Atendimentos</a>
                       <!-- <a class="collapse-item" class="collapse-item" href="forgot-password.html"><i class="fas fa-chart-line"></i>&nbsp;&nbsp;&nbsp;Relatórios de serviço</a> -->
                    </div>
                </div>
            </li>

             <!-- Divider -->
            <hr class="sidebar-divider">

            <?php if ($this->ion_auth->is_admin()){ ?>
                <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-fw fa-folder"></i>
                    <span>Área restrita</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <h6 class="collapse-header">Usuários:</h6>
                        <a class="collapse-item" href="<?php echo base_url('users'); ?>"><i class="fas fa-user"></i>&nbsp;&nbsp;&nbsp;Usuários</a>
                        <a class="collapse-item" href="<?php echo base_url('sectors'); ?>"><i class="fas fa-id-card-alt"></i>&nbsp;&nbsp;&nbsp;Setores</a>
                        <h6 class="collapse-header">Equipamentos:</h6>
                        <a class="collapse-item" href="<?php echo base_url('automations'); ?>"><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;&nbsp;Automações</a>
                        <a class="collapse-item" href="<?php echo base_url('typesidentifier'); ?>"><i class="fas fa-id-card"></i>&nbsp;&nbsp;&nbsp;Identificadores</a>
                        <a class="collapse-item" href="<?php echo base_url('pumpsbrand'); ?>"><i class="fas fa-gas-pump"></i>&nbsp;&nbsp;&nbsp;Marcas de bombas</a>
                        <h6 class="collapse-header">Serviços:</h6>
                        <a class="collapse-item" href="<?php echo base_url('calltypes'); ?>"><i class="fas fa-phone-alt"></i>&nbsp;&nbsp;&nbsp;Tipos de chamado</a>
                        <a class="collapse-item" href="<?php echo base_url('origins'); ?>"><i class="fas fa-clipboard"></i>&nbsp;&nbsp;&nbsp;Tipos de origens</a>
                        <a class="collapse-item" href="<?php echo base_url('priorities'); ?>"><i class="fab fa-product-hunt"></i>&nbsp;&nbsp;&nbsp;Prioridades</a>
                        <a class="collapse-item" href="<?php echo base_url('status'); ?>"><i class="fas fa-arrow-right"></i>&nbsp;&nbsp;&nbsp;Status</a>
                        <h6 class="collapse-header">Clientes/ Parceiros:</h6>
                        <a class="collapse-item" href="<?php echo base_url('typescompany'); ?>"><i class="fas fa-building"></i>&nbsp;&nbsp;&nbsp;Empresas</a>
                    </div>
                </div>
            </li>
            <hr class="sidebar-divider d-none d-md-block">
            <?php } ?>

            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('indicators'); ?>">
                <i class="fas fa-chart-line"></i>
                    <span>Indicadores</span></a>
            </li>

            <hr class="sidebar-divider d-none d-md-block">

            <div class="text-center d-none d-md-inline">
                <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>


            <div class="sidebar-card">
                <img class="sidebar-card-illustration mb-2" src="<?= base_url('')?>/public/img/logo_companytec.svg" alt="...">
                <p class="text-center mb-2">Acesso rápido ao site Companytec.</p>
                <a class="btn btn-success btn-sm" href="https://www.companytec.com.br">Acesse</a>
            </div>

        </ul>
        <div id="content-wrapper" class="d-flex flex-column">         