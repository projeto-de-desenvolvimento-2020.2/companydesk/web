<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('calleds'); ?>">Chamados de serviço</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_add_called">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Cliente</label>
                                <input type="text" class="form-control" id="calleds_customers_end_id_ac"
                                    value="<?php echo set_value('calleds_customers_end_id');?>">
                                <?php echo form_error('calleds_customers_end_id','<small class="form-text text-danger">','</small>')?>
                                <input type="hidden" name="calleds_customers_end_id" id="calleds_customers_end_id">
                        </div>
                        <div class="col-md-6">
                            <label>Parceiro</label>
                                <input type="text" class="form-control"  id="calleds_customers_partner_id_ac"
                                    value="<?php echo set_value('calleds_customers_partner_id');?>">
                                <?php echo form_error('calleds_customers_partner_id','<small class="form-text text-danger">','</small>')?>  
                                <input type="hidden" name="calleds_customers_partner_id" id="calleds_customers_partner_id">                              
                        </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-md-3">                            
                            <label>Prioridade</label>
                            <select class="custom-select" name="calleds_priorities_id">
                                <?php foreach($priorities as $priority): ?>
                                    <option value="<?php echo $priority->priorities_id ?>"><?php echo $priority->priorities_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="calleds_users_id" disabled>
                                    <?php $user = $this->ion_auth->user()->row(); ?>
                                    <option value="<?php echo $user->id ?>"><?php echo $user->first_name;?></option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Automação</label>
                            <select class="custom-select" name="calleds_automations_id">
                                <?php foreach($automations as $automation): ?>
                                    <option value="<?php echo $automation->automations_id ?>"><?php echo $automation->automations_model ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de identificador</label>
                            <select class="custom-select" name="calleds_types_identifier_id">
                                <?php foreach($types_identifier as $type_identifier): ?>
                                    <option value="<?php echo $type_identifier->types_identifier_id ?>"><?php echo $type_identifier->types_identifier_description ?></option>
                                <?php endforeach; ?>
                            </select>                            
                        </div>
                    </div>              
                    <div class="form-group row">     
                        <div class="col-md-4">
                            <label>Status</label>
                            <select class="custom-select" name="calleds_status_id">
                                <?php foreach($status as $statu): ?>
                                    <option value="<?php echo $statu->status_id ?>"><?php echo $statu->status_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>                   
                        <div class="col-md-4">
                            <label>Início</label>
                            <input type="datetime-local" class="form-control" name="calleds_start" 
                                value="<?php echo set_value('calleds_start');?>">
                            <?php echo form_error('calleds_start','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Final</label>
                            <input type="datetime-local" class="form-control" name="calleds_end" 
                                value="<?php echo set_value('calleds_end');?>">
                            <?php echo form_error('calleds_end','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>                   
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="calleds_description"
                                rows="12"><?php echo set_value('calleds_description');?></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <button type="submit" class="btn btn-primary btn-sm ml-3"><i class="far fa-save"></i>&nbsp;&nbsp; Salvar</button>                                
                        <a title="Voltar" href="<?php echo base_url('calleds');?>"
                        class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>