<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">
            <div class="card-body">
                <form method="POST" name="form_edit">
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label>Razão Social</label>
                            <input type="text" class="form-control" name="social_reason"
                                value="<?php echo $customer->customers_social_reason;?>" readonly>
                            <?php echo form_error('social_reason','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Nome Fantasia</label>
                            <input type="text" class="form-control" name="fantasy_name"
                                placeholder="Nome Fantasia" value="<?php echo $customer->customers_fantasy_name;?>" readonly>
                            <?php echo form_error('fantasy_name','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>CNPJ</label>
                            <input type="text" class="form-control cnpj" name="cnpj" placeholder="CNPJ"
                                value="<?php echo $customer->customers_cnpj;?>" readonly>
                            <?php echo form_error('cnpj','<small class="form-text text-danger">','</small>')?>
                        </div>
                          <div class="col-md-3">
                            <label>Inscrição estadual</label>
                            <input type="text" class="form-control" name="state_registration" placeholder="Inscrição estadual"
                                value="<?php echo $customer->customers_state_registration;?>" readonly>
                            <?php echo form_error('state_registration','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-4">
                            <label>Telefone fixo</label>
                            <input type="text" class="form-control phone_with_ddd" name="telephone_fix" placeholder="Telefone fixo"
                                value="<?php echo $customer->customers_telephone_fix;?>" readonly>
                            <?php echo form_error('telephone_fix','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Telefone móvel</label>
                            <input type="text" class="form-control sp_celphones" name="telephone"
                                placeholder="Telefone móvel" value="<?php echo $customer->customers_telephone;?>" readonly>
                            <?php echo form_error('telephone','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>E-mail</label>
                            <input type="email" class="form-control" name="email" placeholder="E-mail"
                                value="<?php echo $customer->customers_email;?>" readonly>
                            <?php echo form_error('email','<small class="form-text text-danger">','</small>')?>
                        </div>                          
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-3">
                            <label>Endereço</label>
                            <input type="text" class="form-control" name="address" placeholder="Endereço"
                                value="<?php echo $customer->customers_address;?>" readonly>
                            <?php echo form_error('address','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-1">
                            <label>Número</label>
                            <input type="text" class="form-control" name="address_number"
                                placeholder="Número" value="<?php echo $customer->customers_address_number;?>" readonly>
                            <?php echo form_error('address_number','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-2">
                            <label>Complemento</label>
                            <input type="text" class="form-control" name="address_complement" placeholder="Complemento"
                                value="<?php echo $customer->customers_address_complement;?>" readonly>
                            <?php echo form_error('address_complement','<small class="form-text text-danger">','</small>')?>
                        </div>
                          <div class="col-md-2">
                            <label>Cidade</label>
                            <input type="text" class="form-control" name="city" placeholder="Cidade"
                                value="<?php echo $customer->customers_city;?>" readonly>
                            <?php echo form_error('city','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-2">
                            <label>CEP</label>
                            <input type="text" class="form-control cep" name="cep" placeholder="CEP"
                                value="<?php echo $customer->customers_cep;?>" readonly>
                            <?php echo form_error('cep','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-2">
                            <label>Estado</label>
                            <input type="text" class="form-control fu" name="federative_unit"
                                placeholder="Estado" value="<?php echo $customer->customers_federative_unit;?>" readonly>
                            <?php echo form_error('federative_unit','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">                                                
                        <div class="col-md-3">
                            <label>País</label>
                            <input type="text" class="form-control" name="country" placeholder="País"
                                value="<?php echo $customer->customers_country;?>" readonly>
                            <?php echo form_error('country','<small class="form-text text-danger">','</small>')?>
                        </div>
                          <div class="col-md-3">
                            <label>Ativo</label>
                            <input type="text" class="form-control" name="defaulting" placeholder="Ativo"
                                value="<?php echo $customer->customers_defaulting;?>" readonly>
                            <?php echo form_error('defaulting','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Número de bombas</label>
                            <input type="text" class="form-control" name="number_pumps" placeholder="Número de bombas"
                                value="<?php echo $customer->customers_number_pumps;?>" readonly>
                            <?php echo form_error('number_pumps','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de automação</label>
                            <input type="text" class="form-control" name="automations_id" placeholder="Tipo de automação"
                                value="<?php echo $customer->customers_automations_id;?>" readonly>
                            <?php echo form_error('automations_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-group row mt-3">
                        <div class="col-md-3">
                            <label>Tipo de identificador</label>
                            <input type="text" class="form-control" name="types_identifier_id" placeholder="Tipo de identificador"
                                value="<?php echo $customer->customers_types_identifier_id;?>" readonly>
                            <?php echo form_error('types_identifier_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Marcas das bombas</label>
                            <input type="text" class="form-control" name="pumps_brand_id" placeholder="Marcas das bombas"
                                value="<?php echo $customer->customers_pumps_brand_id;?>" readonly>
                            <?php echo form_error('pumps_brand_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de cliente</label>
                            <input type="text" class="form-control" name="types_company_id" placeholder="Tipo de cliente"
                                value="<?php echo $customer->customers_types_company_id;?>" readonly>
                            <?php echo form_error('types_company_id','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>
                    <div class="form-gropu row mt-3 mb-3">
                        <div class="col-md-12">
                            <label>Comentários</label>
                            <textarea class="form-control" name="comments" rows="10" readonly
                                placeholder="Comentários"><?php echo $customer->customers_comments;?></textarea>  
                                <?php echo form_error('comments','<small class="form-text text-danger">','</small>')?>                                                      
                        </div>                        
                    </div>            
                        <a title="Voltar" href="<?php echo base_url('calleds');?>"
                            class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                    </div>
        </form>
    </div>
</div>