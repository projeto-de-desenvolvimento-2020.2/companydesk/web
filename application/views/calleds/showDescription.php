<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('calleds'); ?>">Chamados de serviço</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">            
            <div class="card-body">
                <form method="POST" name="form_show_called">
                    <?php foreach($customers as $customer): ?>
                        <?php 
                            if($called->calleds_customers_end_id == $customer->customers_id){
                                $called_customer_end = $customer->customers_social_reason;
                            }    
                            if($called->calleds_customers_partner_id == $customer->customers_id){
                                $called_customer_partner = $customer->customers_social_reason;
                            }
                        ?>
                    <?php endforeach; ?>  
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label>Cliente</label>
                            <div class=" input-group">
                                <input type="text" class="form-control" name="calleds_customers_end_id"
                                    value="<?php echo $called_customer_end;?>" readonly>
                                <?php echo form_error('calleds_customers_end_id','<small class="form-text text-danger">','</small>')?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label>Parceiro</label>
                            <div class=" input-group">
                                <input type="text" class="form-control" name="calleds_customers_partner_id"
                                    value="<?php echo $called_customer_partner;?>" readonly>
                                <?php echo form_error('calleds_customers_partner_id','<small class="form-text text-danger">','</small>')?>                                
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">                            
                            <label>Prioridade</label>
                            <select class="custom-select" name="calleds_priorities_id" disabled>
                                <?php foreach($priorities as $priority): ?>
                                    <option value="<?php echo $priority->priorities_id ?>"<?php echo ($priority->priorities_id == $called->calleds_priorities_id ? 'selected' : '') ?>><?php echo $priority->priorities_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Usuário</label>
                            <select class="custom-select" name="calleds_users_id" disabled>
                                <?php foreach($users as $user): ?>
                                    <option value="<?php echo $user->id ?>" <?php echo ($user->id == $called->calleds_users_id ? 'selected' : '') ?> ><?php echo $user->first_name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Automação</label>
                            <select class="custom-select" name="calleds_automations_id" disabled>
                                <?php foreach($automations as $automation): ?>
                                    <option value="<?php echo $automation->automations_id ?>"<?php echo ($automation->automations_id == $called->calleds_automations_id ? 'selected' : '') ?>><?php echo $automation->automations_model ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label>Tipo de identificador</label>
                            <select class="custom-select" name="calleds_types_identifier_id" disabled>
                                <?php foreach($types_identifier as $type_identifier): ?>
                                    <option value="<?php echo $type_identifier->types_identifier_id ?>"<?php echo ($type_identifier->types_identifier_id == $called->calleds_types_identifier_id ? 'selected' : '') ?>><?php echo $type_identifier->types_identifier_description ?></option>
                                <?php endforeach; ?>
                            </select>                            
                        </div>
                    </div>
                    <div class="form-group row">   
                        <div class="col-md-4">
                            <label>Status</label>
                            <select class="custom-select" name="calleds_status_id" disabled>
                                <?php foreach($status as $statu): ?>
                                    <option value="<?php echo $statu->status_id ?>"><?php echo $statu->status_description ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>                     
                        <div class="col-md-4">
                            <label>Início</label>
                            <input type="text" class="form-control" name="calleds_start" 
                                value="<?php echo $called->calleds_start;?>" readonly>
                            <?php echo form_error('calleds_start','<small class="form-text text-danger">','</small>')?>
                        </div>
                        <div class="col-md-4">
                            <label>Final</label>
                            <input type="text" class="form-control" name="calleds_end" 
                                value="<?php echo $called->calleds_end;?>" readonly>
                            <?php echo form_error('calleds_end','<small class="form-text text-danger">','</small>')?>
                        </div>
                    </div>                   
                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="exampleFormControlTextarea1">Descrição</label>
                            <textarea class="form-control" name="calleds_description"
                                rows="12" readonly><?php echo $called->calleds_description;?></textarea>
                        </div>
                    </div>
                    <fieldset class="mt-4 border p-2">
                    <legend class="font-small"><i class="fas fa-pen"></i>&nbsp; Atividades relacionadas</legend>
                        <div class="form-group row mt-3">
                        </div>
                        <div class="table-responsive">
                                <table class="table table-striped table-borderless"> 
                                    <thead>
                                        <tr>
                                            <th class="text-center">Id</th>
                                            <th class="text-center">Inicio</th>
                                            <th class="text-center">Final</th>                                
                                            <th class="text-center">Chamado</th> 
                                            <th class="text-center">Cliente</th>                               
                                            <th class="text-center">Parceiro</th>
                                            <th class="text-center">Usuário</th>
                                            <th class="text-center">Origem</th>
                                            <th class="text-center">Status</th>
                                            <th class="text-center no-sort">Ações</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($activitiesCalled as $activityCalled):?>
                                            <tr>
                                            <td class="text-center"> <a href="<?php echo base_url('activities/showDescription/'.$activityCalled->activities_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $activityCalled->activities_id ?></td>
                                            <td class="text-center"> <?php echo date("d/m/Y H:i:s",strtotime($activityCalled->activities_start)) ?></td>
                                            <td class="text-center"> <?php echo ($activityCalled->activities_end == '') ? '---' : date("d/m/Y H:i:s",strtotime($activityCalled->activities_end))?></td>
                                            <td class="text-center"> <a href="<?php echo base_url('activities/showCalled/'.$activityCalled->activities_calleds);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $activityCalled->activities_calleds ?></td>
                                            <td class="text-center"> <a href="<?php echo base_url('activities/showCustomer/'.$activityCalled->activities_customers_end_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $activityCalled->activities_customers_end ?></td>
                                            <td class="text-center"> <a href="<?php echo base_url('activities/showCustomer/'.$activityCalled->activities_customers_partner_id);?>"><i class="far fa-arrow-alt-circle-right"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $activityCalled->activities_customers_partner ?></td>                                
                                            <td class="text-center"> <?php echo $activityCalled->activities_user ?></td>
                                            <td class="text-center"> <?php echo $activityCalled->activities_origins ?></td>
                                            <td class="text-center"> <?php   
                                                if($activityCalled->activities_status == 'Fechado'){
                                                    echo '<span class="badge badge-danger btn-sm">'.$activityCalled->activities_status.'</span>';
                                                }else{
                                                    echo '<span class="badge badge-primary btn-sm">'.$activityCalled->activities_status.'</span>';
                                                }
                                            ?></td>
                                            <td class="text-center">
                                                <button class="btn brn-sm btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-bars"></i>
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                <?php if($activityCalled->activities_status !== 'Fechado'){ ?>
                                                    <a class="dropdown-item" title="Editar" href="<?php echo base_url('activities/edit/'.$activityCalled->activities_id); ?>">Editar</a>     
                                                    <?php } ?>
                                                    <a class="dropdown-item" tittle="Atividades relacionados" href="<?php echo base_url('activities/relatedAttendances/'.$activityCalled->activities_id);?>">Atendimentos relacionados</a>
                                                </div> 
                                            </td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </fieldset>
                    <div class="form-group row mt-3">
                        <a title="Voltar" href="<?php echo base_url('calleds');?>"
                            class=" btn btn-success btn-sm ml-3"><i class="fas fa-arrow-left"></i>&nbsp; Voltar</a>
                            <?php if($called->calleds_status_id != 2){ ?>
                                <a title="Editar" href="<?php echo base_url('calleds/edit/'.$called->calleds_id);?>"
                                    class=" btn btn-primary btn-sm ml-3"><i class="fas fa-edit"></i>&nbsp; Editar</a>
                                <a title="Nova atividade" href="<?php echo base_url('activities/addWithCalled/'.$called->calleds_id);?>"
                                    class=" btn btn-primary btn-sm float-right ml-3"><i class="fas fa-plus-circle"></i>&nbsp; Nova atividade</a>
                            <?php } ?>  
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>