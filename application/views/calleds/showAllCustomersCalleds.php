<?php $this->load->view('layout/sidebar'); ?>
<div id="content">
    <?php $this->load->view('layout/navbar');?>
    <div class="container-fluid">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?php echo base_url('/'); ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><?php echo $title ?></li>
            </ol>
        </nav>
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <a title="Cadastrar novo cliente final" href="<?php echo base_url('customers/add')?>"
                    class=" btn btn-success btn-sm float-right"><i class="fas fa-user-plus"></i>&nbsp; Novo</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Razão social</th>
                                <th class="text-center">Nome fantasia</th>
                                <th class="text-center">CNPJ</th>
                                <th class="text-center">Email</th>
                                <th class="text-center">Telefone fixo</th>
                                <th class="text-center">Telefone móvel</th>
                                <th class="text-center">Estado</th>                            
                                <th class="text-center">Ativo</th>                            
                                <th class="text-center no-sort">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($customers as $customer):?>
                                <?php if($customer->customers_defaulting == 1): ?>
                                    <tr>
                                        <td class="text-center"> <?php echo $customer->customers_id ?></td>
                                        <td class="text-center"> <?php echo $customer->customers_social_reason?></td>
                                        <td class="text-center"> <?php echo $customer->customers_fantasy_name ?></td>
                                        <td class="text-center"> <?php echo $customer->customers_cnpj ?></td>
                                        <td class="text-center"> <?php echo $customer->customers_email ?></td>
                                        <td class="text-center"> <?php echo $customer->customers_telephone_fix ?></td>
                                        <td class="text-center"> <?php echo $customer->customers_telephone ?></td>
                                        <td class="text-center"> <?php echo $customer->customers_federative_unit ?></td>                                
                                        <td class="text-center pr-4"> <?php echo ($customer->customers_defaulting == 1 ? '<span class="badge badge-primary btn-sm">Sim</span>':'<span class="badge badge-danger">Não</span>') ?></td>
                                        <td class="text-center">
                                            <a title="Editar"
                                                href="<?php echo base_url('customers/edit/'.$customer->customers_id); ?>"
                                                class="btn brn-sm btn-primary"><i class="fas fa-plus"></i></a>                                    
                                        </td>
                                    </tr>  
                                <?php endif; ?>                      
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>