<?php

defined('BASEPATH') OR exit('Ação não permitida.');

class Home_model extends CI_Model{

    public function attendancesLast30Days($user_id = NULL){
        $data = @date('Y-m-d');
        $sql = $this->db->query("
            SELECT COUNT(attendances_id) as numberAttendances FROM attendances WHERE  attendances_users_id = '.$user_id.' AND CAST(attendances_start AS date) >= CAST(now()- interval 30 day AS date)
        ");
        return $sql->result();
    }

    public function activitiesLast30Days($user_id = NULL){     
        $data = @date('Y-m-d');
        $sql = $this->db->query("
            SELECT COUNT(activities_id) as numberActivities FROM activities WHERE  activities_users_id = '.$user_id.' AND CAST(activities_start AS date) >= CAST(now()- interval 30 day AS date)
        ");
        return $sql->result();
    }

    public function calledsLast30Days($user_id = NULL){
        $data = @date('Y-m-d');
        $sql = $this->db->query("
            SELECT COUNT(calleds_id) as numberCalleds FROM calleds WHERE  calleds_users_id = '.$user_id.' AND CAST(calleds_start AS date) >= CAST(now()- interval 30 day AS date)
        ");
        return $sql->result();
    }

    public function lastThreeCalleds(){
        $this->db->select([
            'calleds.*',
            'calleds.calleds_end as final',

            'customers.customers_id',
            'customers.customers_fantasy_name as customers_end',

            'cus2.customers_id',
            'cus2.customers_fantasy_name as customers_partner',

            'users.id',
            'users.first_name as user',
        ]);
        $this->db->join('customers','customers.customers_id = calleds_customers_end_id', 'LEFT');
        $this->db->join('customers cus2','cus2.customers_id = calleds_customers_partner_id', 'LEFT');
        $this->db->join('users','users.id = calleds_users_id', 'LEFT');
        $this->db->order_by('calleds_id', 'DESC');
        $this->db->limit(3);
        return $this->db->get('calleds')->result();
    }

    public function calledsOpenToday($user_id = NULL){
        $data = @date('Y-m-d');
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query('
                select count(calleds_id) as calledsOpeToday from calleds where calleds_start = '.$data
            );
        }else{
            $sql = $this->db->query('
                select count(calleds_id) as calledsOpeToday from calleds where calleds_start = '.$data.' and calleds_users_id = '.$user_id
            );
        }
        return $sql->result();
    }

    public function calledsCloseToday($user_id = NULL){
        $data = @date('Y-m-d');
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query('
                select count(calleds_id) as calledsClosToday from calleds where calleds_end = '.$data
            );
        }else{
            $sql = $this->db->query('
                select count(calleds_id) as calledsClosToday from calleds where calleds_end = '.$data.' and calleds_users_id = '.$user_id
            );
        }        
        return $sql->result();
    }

    public function attendancesToday($user_id = NULL){
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query("
                SELECT COUNT(attendances_id) as attendancesToday FROM attendances WHERE CAST(attendances_start AS date) = CAST(now() AS date)
            ");
        }else{
            $sql = $this->db->query("
                SELECT COUNT(attendances_id) as numberAttendances FROM attendances WHERE attendances_users_id = '.$user_id.' AND CAST(attendances_start AS date) = CAST(now() AS date)
            ");
        }        
        return $sql->result();
    }

    public function activitiesToday($user_id = NULL){
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as activitiesToday FROM activities WHERE CAST(activities_start AS date) = CAST(now() AS date)
            ");
        }else{
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as activitiesToday FROM activities WHERE activities_users_id = '.$user_id.' AND CAST(activities_start AS date) = CAST(now() AS date)
            ");
        }        
        return $sql->result();
    }

    public function calledsToday($user_id = NULL){
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query("
                SELECT COUNT(calleds_id) as calledsToday FROM calleds WHERE CAST(calleds_start AS date) = CAST(now() AS date)
            ");
        }else{
            $sql = $this->db->query("
                SELECT COUNT(calleds_id) as calledsToday FROM calleds WHERE calleds_users_id = '.$user_id.' AND CAST(calleds_start AS date) = CAST(now() AS date)
            ");
        }        
        return $sql->result();
    }


    //Alertas
    public function  activitiesLowPriorityWinning(){
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as numberActivitiesLow FROM activities WHERE activities_end IS NULL AND activities_priorities_id = 3 AND CAST(activities_start AS date) = CAST(now()- interval 30 day AS date)
            ");
        }else{
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as numberActivitiesLow FROM activities WHERE activities_end IS NULL AND activities_priorities_id = 3 AND activities_users_id = '.$user_id.' AND CAST(activities_start AS date) = CAST(now()- interval 30 day AS date)
            ");
        }        
        return $sql->row();
    }

    public function  activitiesMediumPriorityWinning(){
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as numberActivitiesMedium FROM activities WHERE activities_end IS NULL AND activities_priorities_id = 2 AND CAST(activities_start AS date) = CAST(now()- interval 15 day AS date)
            ");
        }else{
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as numberActivitiesMedium FROM activities WHERE activities_end IS NULL AND activities_priorities_id = 2 AND activities_users_id = '.$user_id.' AND CAST(activities_start AS date) = CAST(now()- interval 15 day AS date)
            ");
        }        
        return $sql->row();
    }

    public function  activitiesHighPriorityWinning(){
        if($this->ion_auth->get_users_groups()->row()->id == 1){
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as numberActivitiesHigh FROM activities WHERE activities_end IS NULL AND activities_priorities_id = 1 AND CAST(activities_start AS date) = CAST(now()- interval 7 day AS date)
            ");
        }else{
            $sql = $this->db->query("
                SELECT COUNT(activities_id) as numberActivitiesHigh FROM activities WHERE activities_end IS NULL AND activities_priorities_id = 1 AND activities_users_id = '.$user_id.' AND CAST(activities_start AS date) = CAST(now()- interval 7 day AS date)
            ");
        }        
        return $sql->row();
    }
}